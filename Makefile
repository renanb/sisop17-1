#
# Makefile de EXEMPLO
#
# OBRIGATÓRIO ter uma regra "all" para geração da biblioteca e de uma
# regra "clean" para remover todos os objetos gerados.
#
# É NECESSARIO ADAPTAR ESSE ARQUIVO de makefile para suas necessidades.
#  1. Cuidado com a regra "clean" para não apagar o "support.o"
#
# OBSERVAR que as variáveis de ambiente consideram que o Makefile está no diretótio "cthread"
# 

CC=gcc
LIB_DIR=./lib
INC_DIR=./include
BIN_DIR=./bin
SRC_DIR=./src

all: libcthread

libcthread: cthread
	$(AR) crs $(LIB_DIR)/libcthread.a $(BIN_DIR)/cthread.o $(LIB_DIR)/support.o $(BIN_DIR)/cdata.o

cthread:
	$(CC) -c -g $(SRC_DIR)/cthread.c -I$(INC_DIR) -o $(BIN_DIR)/cthread.o -m32 -Wall 
	$(CC) -c -g $(SRC_DIR)/cdata.c -I$(INC_DIR) -o $(BIN_DIR)/cdata.o -m32 -Wall

exemplo: ./exemplos/exemplo.c libcthread
	$(CC) -o exemplo ./exemplos/exemplo.c -L$(LIB_DIR) -lcthread -Wall -m32

mandel: ./exemplos/mandel.c libcthread
	$(CC) -o mandel ./exemplos/mandel.c -L$(LIB_DIR) -lcthread -Wall -m32

barbeiro: ./exemplos/barbeiro.c libcthread
	$(CC) -o barbeiro ./exemplos/barbeiro.c -L$(LIB_DIR) -lcthread -Wall -m32

prodcons: ./exemplos/prodcons.c libcthread
	$(CC) -o prodcons ./exemplos/prodcons.c -L$(LIB_DIR) -lcthread -Wall -m32

filosofos: ./exemplos/filosofos.c libcthread
	$(CC) -o filosofos ./exemplos/filosofos.c -L$(LIB_DIR) -lcthread -Wall -m32

clean:
	rm -rf $(LIB_DIR)/*.a $(BIN_DIR)/*.o $(SRC_DIR)/*~ $(INC_DIR)/*~ *~

