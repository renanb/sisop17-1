#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/cdata.h"


#define debug_cdata printf

//Função que realiza o término de uma thread (serve também para pegar o contexto do local onde uma thread deve terminar, para isso que serve o retorno)
void terminate()
{
//	debug_cdata("Entrou na funcao TERMINATE!\n");

	terminate_context = (ucontext_t*)malloc(sizeof(ucontext_t)); //com alocação dinamica, a memória não é liberada qnd tu sai da função
	TCB_t* thread;

	//variavel para identificar se a função terminate deve só retornar contexto ou de fato terminar a thread
	int is_terminate_context;

	is_terminate_context = 1;

	getcontext(terminate_context);
	if(is_terminate_context == 1) {
		is_terminate_context = 0;
//		debug_cdata("Terminate retornou o contexto!\n");
		return;
	}
//	debug_cdata("Entrou na funcao TERMINATE via UC_LINK!\n");
	
	find_current_thread(&thread, threads);
	thread->state = PROCST_TERMINO;

	//Toda vez que uma thread termina a gente checa se tinha alguma bloqueada esperando por essa
	is_block(threads_block, thread->tid, prio0, prio1, prio2, prio3);

//	debug_cdata("Saiu da funcao TERMINATE\n!");

	swap(thread, prio0, prio1, prio2, prio3);
}

// Função que encontra a thread que está executando
int find_current_thread(TCB_t** current_thread, PFILA2 threads)
{
//	debug_cdata("Entrou na funcao FIND_CURRENT_THREAD!\n");
	
	FirstFila2(threads);

	// Percorre a fila de threads
	do
	{
		TCB_t* t = (TCB_t*) GetAtIteratorFila2(threads);
		// Confere se é uma thread que está executando e não é a main
		if(t->state == PROCST_EXEC)
		{
			*current_thread = t;	

//			debug_cdata("Saiu com sucesso da funcao FIND_CURRENT_THREAD!\n");
			return 0;
		}
	}while(NextFila2(threads) == 0);
	
//	debug_cdata("Saiu sem sucesso da funcao FIND_CURRENT_THREAD!\n");
	
	return -1;
} 

// Função que seleciona qual a próxima thread da fila de aptos deve ser executda
int select_next_thread(TCB_t** thread, PFILA2 prio0, PFILA2 prio1, PFILA2 prio2, PFILA2 prio3)
{
//	debug_cdata("Entrou na funcao SELECT_NEXT_THREAD!\n");
	
	// Se a fila de aptos de prioridade 0 não estiver vazia, escolhe a primeira thread dessa fila
	if (FirstFila2(prio0)==0)
	{
		*thread = (TCB_t*) GetAtIteratorFila2(prio0);
	// Se a fila de aptos de prioridade 1 não estiver vazia, escolhe a primeira thread dessa fila
	}else if(FirstFila2(prio1)==0)
	{
		*thread = (TCB_t*) GetAtIteratorFila2(prio1);
	// Se a fila de aptos de prioridade 2 não estiver vazia, escolhe a primeira thread dessa fila
	}else if(FirstFila2(prio2)==0)
	{
		*thread = (TCB_t*) GetAtIteratorFila2(prio2);
	// Se a fila de aptos de prioridade 3 não estiver vazia, escolhe a primeira thread dessa fila
	}else if(FirstFila2(prio3)==0)
	{
		*thread = (TCB_t*) GetAtIteratorFila2(prio3);
	// Se não existir nenhuma thread no estado apto, retorna erro
	}else{
//		debug_cdata("Saiu sem sucesso da funcao SELECT_NEXT_THREAD!\n");
		return -1;
	}
	
//	debug_cdata("Saiu com sucesso da funcao SELECT_NEXT_THREAD!\n");

	return 0;
}


/*
- Funcao que verifica se a thread existe.
- Recebe como parâmetro a lista de threads e o ID da thread a ser procurada
- Retorno: caso encontre retorna a thread, se nao encontrar retorna NULL
*/
TCB_t* tid_exists (PFILA2 th_list, int tid) {

	//debug_cdata("Entrou na funcao TID_EXISTS! Procurando pelo id: %d\n", tid);

	TCB_t *aux = NULL;
	FirstFila2(th_list);

	while(GetAtIteratorFila2(th_list) != NULL) {
		aux = (TCB_t*) GetAtIteratorFila2(th_list);

		if(aux->tid == tid){
//			debug_cdata("Saiu com sucesso da funcao TID_EXISTS!\n");
			return aux;
		}

		NextFila2(th_list);
	}

//	debug_cdata("Saiu sem sucesso da funcao TID_EXISTS!\n");
	return NULL;
}


/*
- Funcao que verifica se a thread existe na lista de bloqueados.
- Recebe como parâmetro a lista de threads e o ID da thread a ser procurada
- Retorno: caso encontre retorna a thread bloqueada, se nao encontrar retorna NULL
*/
TCB_block* tid_block_exists (PFILA2 th_list, int tid) {
	
//	debug_cdata("Entrou na funcao TID_BLOQ_EXISTS!\n");

	TCB_block *aux = NULL;
	FirstFila2(th_list);

	while(GetAtIteratorFila2(th_list) != NULL) {
		aux = (TCB_block*) GetAtIteratorFila2(th_list);

		if(aux->tid == tid){
//			debug_cdata("Saiu com sucesso da funcao TID_BLOQ_EXISTS!\n");
			return aux;
		}

		NextFila2(th_list);
	}

//	debug_cdata("Saiu sem sucesso da funcao TID_BLOQ_EXISTS!\n");
	return NULL;
}


//Funcao que realiza o desbloqueio da thread passada por parâmetro
void unlock_join(TCB_t* thread, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3) 
{
	thread->state = PROCST_APTO;
	insert_apto(thread, p0, p1, p2, p3);
}


/*Funcao que verifica se há threads para desbloquear
Entrada: lista de bloqueados e tid da thread que terminou sua execução*/
void is_block(PFILA2 th_bk, int tid, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3)
{
	TCB_block* t_block;

	t_block = tid_block_exists(th_bk, tid);

	if(t_block)
	{
		DeleteAtIteratorFila2(th_bk);
		unlock_join(t_block->thread_blocked, p0, p1, p2, p3);
	}
}


//Função que insere a thread passado por parâmetro na fila de prioridades correta
void insert_apto(TCB_t* thread, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3)
{
	if(thread->prio == 0)
		AppendFila2(p0, thread);
	else if(thread->prio == 1)
		AppendFila2(p1, thread);
	else if(thread->prio == 2)
		AppendFila2(p2, thread);
	else if(thread->prio == 3)
		AppendFila2(p3, thread);
}


//Função que realiza o swap
void swap(TCB_t* current_thread, PFILA2 prio0, PFILA2 prio1, PFILA2 prio2, PFILA2 prio3)
{	
	TCB_t* next_thread;

	//Procura a proxima thread a ser executada
	select_next_thread(&next_thread, prio0, prio1, prio2, prio3);
	
	//Muda estado da thread para executando
	next_thread->state = PROCST_EXEC;
	
	//Tira a current_thread da fila dela	
	if(next_thread->prio == 0){	
		tid_exists(prio0, next_thread->tid);
		DeleteAtIteratorFila2(prio0);
	}
	else if(next_thread->prio == 1){	
		tid_exists(prio1, next_thread->tid);
		DeleteAtIteratorFila2(prio1);
	}
	else if(next_thread->prio == 2){	
		tid_exists(prio2, next_thread->tid);
		DeleteAtIteratorFila2(prio2);
	}
	else if(next_thread->prio == 3){	
		tid_exists(prio3, next_thread->tid);
		DeleteAtIteratorFila2(prio3);
	}

	//Muda o contexto atual com o proximo contexto
	swapcontext(&(current_thread->context),&(next_thread->context));
}


//Função utilizada na hora de debugar, imprime os tids de uma lista
void print_list(PFILA2 list)
{
	FirstFila2(list);
	do{
		debug_cdata("TID: %d\n", ((TCB_t*)GetAtIteratorFila2(list))->tid);
	}while(NextFila2(list) == 0);
}
