#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/cdata.h"
#include "../include/cthread.h"

//variavel para identificar se a thread criada eh a main
int is_main_thread=0;
//variavel para controlar o tid
int TID=0;
//fila de threads
PFILA2 threads;
//filas de prioridades (consequentemente de aptos)
PFILA2 prio0;
PFILA2 prio1;
PFILA2 prio2;
PFILA2 prio3;
//fila de bloqueados
PFILA2 threads_block;
//variavel para identificar o final do contexto
ucontext_t *terminate_context = NULL;

#define debug printf


int ccreate (void* (*start)(void*), void *arg, int prio)
{
//	debug("Entrou na funcao CCREATE!\n");

	char *thread_stack = (char*)malloc(sizeof(char)*SIGSTKSZ);
	TCB_t* thread_main =  malloc(sizeof(TCB_t));
	TCB_t* thread =  malloc(sizeof(TCB_t));
	
	if(is_main_thread == 0)
	{
		is_main_thread = 1; //marca que a main foi criada

		//cria a main
		thread_main -> tid = 0;
		thread_main -> state = PROCST_EXEC;
		thread_main -> prio = 0;
		
		threads = (FILA2*)malloc(sizeof(FILA2));
		prio0 = (FILA2*)malloc(sizeof(FILA2));
		prio1 = (FILA2*)malloc(sizeof(FILA2));
		prio2 = (FILA2*)malloc(sizeof(FILA2));
		prio3 = (FILA2*)malloc(sizeof(FILA2));
		threads_block = (FILA2*)malloc(sizeof(FILA2));

		//cria a fila principal de threads
		if (CreateFila2(threads)) {
			debug("ERRO AO CRIAR FILA DE THREADS!\n");
			exit(0);
		}
		if(FirstFila2(threads) != 0){
			AppendFila2(threads, thread_main); //a main é colocada na fila de threads (e obrigatóriamente deve ser a primeira thread lá)
		}
		else {
			debug("FILA DE THREADS NAO ESTA VAZIA AO CRIAR A MAIN!\n");
			exit(0);
		}
			

		//cria as filas de prioridades
		if( CreateFila2(prio0) != 0){
			debug("ERRO AO CRIAR FILA DE PRIORIDADES 0!\n");
			exit(0);
		}
		if( CreateFila2(prio1) != 0){
			debug("ERRO AO CRIAR FILA DE PRIORIDADES 1!\n");
			exit(0);
		}
		if( CreateFila2(prio2) != 0){
			debug("ERRO AO CRIAR FILA DE PRIORIDADES 2!\n");
			exit(0);
		}
		if( CreateFila2(prio3) != 0){
			debug("ERRO AO CRIAR FILA DE PRIORIDADES 3!\n");
			exit(0);
		}

		//cria a fila de bloqueados
		if( CreateFila2(threads_block) != 0){
			debug("ERRO AO CRIAR FILA DE BLOQUEADOS!\n");
			exit(0);
		}

		//só é preciso achar o contexto de terminação das threads uma unica vez
		terminate();
	}
	
    getcontext(&(thread->context));

		
	//cria a thread atual:final do contexto dado pela terminate e estado como apto
	(thread->context).uc_link          = terminate_context;     
    (thread->context).uc_stack.ss_sp   = thread_stack;
    (thread->context).uc_stack.ss_size = SIGSTKSZ;

	makecontext(&(thread->context), (void (*)(void)) start, 1, arg);

	TID++;

	thread->tid = TID;
	thread->state = PROCST_APTO;
	thread->prio = prio;

	//add no final da fila de threads
	AppendFila2(threads, thread);

	//add no final da fila da prioridade certa
	if(prio == 0)
		AppendFila2(prio0, thread);
	else if(prio == 1)
		AppendFila2(prio1, thread);
	else if(prio == 2)
		AppendFila2(prio2, thread);
	else if(prio == 3)
		AppendFila2(prio3, thread);

//	debug("Saiu da funcao CCREATE!\n");
//	print_list(threads);
	return thread->tid;
}

int csetprio(int tid, int prio)
{
/*
Verificar se a prioridade está em 0..3
Verifica se existe o id da thread
Se a thread não está em “Apto” altera a prioridade. Se a thread está em “Apto” altera a prioridade, remove a thread da fila e re-insere na fila da nova prioridade.

Retorno:
Quando executada corretamente: retorna 0 (zero)
Caso contrário, retorna um valor negativo.
*/
//	debug("Entrou na funcao CSETPRIO!\n");
	int retorno;
	TCB_t* th;

	if(prio < 0 || prio > 3)
	{
		debug("A prioridade passada está incorreta, deveria ser um número entre 0 e 3.\n");
		retorno = -1;
	}
	else
	{
		if(tid_exists(threads, tid) == NULL)
		{
			debug("A thread não existe. (Ou já terminou ou nunca ainda não foi criada)\n");
			retorno = -1;
		}
		else
		{
			th = tid_exists(threads, tid);

			if(th->state == PROCST_APTO)
			{
				if(th->prio == 0){	
					tid_exists(prio0, tid);
					DeleteAtIteratorFila2(prio0);
				}
				else if(th->prio == 1){	
					tid_exists(prio1, tid);
					DeleteAtIteratorFila2(prio1);
				}
				else if(th->prio == 2){	
					tid_exists(prio2, tid);
					DeleteAtIteratorFila2(prio2);
				}
				else if(th->prio == 3){	
					tid_exists(prio3, tid);
					DeleteAtIteratorFila2(prio3);
				}
				
				th->prio = prio;
			
				insert_apto(th, prio0, prio1, prio2, prio3);
			}
			else			
				th->prio = prio;
		}
	}
//	debug("Saiu da funcao CSETPRIO!\n");

	return retorno;
}


int cyield(void)
{
//	debug("Entrou na funcao CYIELD!\n");
	
	TCB_t* current_thread;
	
	find_current_thread(&current_thread, threads);
	
	// Insere a thread atual em sua fila de aptos correspondente
	insert_apto(current_thread, prio0, prio1, prio2, prio3);
	
	// Muda estado da thread atual(Executando->Apto)
	current_thread->state = PROCST_APTO;
	
//	debug("Saiu da funcao CYIELD!\n");

	//Muda Contexto 
	swap(current_thread, prio0, prio1, prio2, prio3);
	
	return 0;
}

int cjoin(int tid)
{/*	- Checar se a thread com o tid passado existe.
		- Caso não exista: erro
		- Caso exista: Checar se o estado é terminado
			-Caso seja: erro
	-Checa se já existe alguma thread na fila de bloqueadas com o tid da outra
		- Caso exista: erro.
		- Caso não exista: Coloca na fila.
	- Bloqueia a thread que fez cjoin. 
	- Fazer swap (?)
	
Em outra função:
	- Quando a thread terminar a execução da thread 'tid', a thread que fez cjoin volta para o estado Apto.


Quando executada corretamente: retorna 0 (zero)
Caso contrário, retorna um valor negativo.
*/
	TCB_t* expc_thread;
	TCB_t* current_thread;
	TCB_block* th_bck = malloc(sizeof(TCB_block));

//	debug("Entrou na funcao CJOIN!\n");
	
	if (tid_exists(threads, tid) == NULL)
	{
		debug("Thread passada não existe.\n");
		return -1;
	}
	else
	{
		expc_thread = tid_exists(threads, tid);

		if(expc_thread->state == PROCST_TERMINO)
		{
			debug("Thread passada já foi terminada.\n");
			return -1;	
		}
		else
		{
			if(tid_block_exists(threads_block, tid) != NULL)
			{
				debug("Já existe outra thread esperando pela thread que foi passada.\n");
				return -1;	
			}
			else
			{
//				print_list(threads);
				find_current_thread(&current_thread, threads);

				current_thread->state = PROCST_BLOQ;

				//Salva o identificador da thread que ele está esperando e a thread que foi bloqueada
				th_bck->tid = tid;
				th_bck->thread_blocked = current_thread;

				AppendFila2(threads_block, th_bck);

//				debug("Saiu da funcao CJOIN pelo swap!\n");
		
				swap(current_thread, prio0, prio1, prio2, prio3);
				
			}
		}
	}
//	debug("Saiu da funcao CJOIN!\n");
	return 0;
}

int csem_init(csem_t *sem, int count)
{
/*
Inicializa a variável do tipo csem_t
	count: representa a quantidade existente do recurso controlado pelo semáforo. Para realizar exclusão mútua, esse valor inicial da variável semáforo deve ser 1.
	lista: cada variável semáforo deve ter associado uma estrutura que registre as threads que estão bloqueadas, esperando por sua liberação. Na inicialização essa lista deve estar vazia.

Retorno:
Quando executada corretamente: retorna 0 (zero)
Caso contrário, retorna um valor negativo.*/
//	debug("Entrou na funcao CSEM_INIT!\n");
	
	sem->count = count;

	sem->fila = (FILA2*)malloc(sizeof(FILA2));

	if( CreateFila2(sem->fila) != 0){
			debug("ERRO AO CRIAR FILA DO SEMAFORO!\n");
			return -1;
	}

//	debug("Saiu da funcao CSEM_INIT!\n");

	return 0;
}

int cwait(csem_t *sem)
{
/*
- Checa se count <= 0
	- Caso sim: muda o estado da thread p bloqueado
			coloca a thread na fila de bloqueados
- Decrementa count
- Faz o swap (?)

Retorno:
Quando executada corretamente: retorna 0 (zero)
Caso contrário, retorna um valor negativo.
*/

//	debug("Entrou na funcao CWAIT!\n");
	
	TCB_t* current_thread;
	int retorno;	
	
	if(sem->count <= 0)
	{
		find_current_thread(&current_thread, threads);

		current_thread->state = PROCST_BLOQ;

		AppendFila2(sem->fila, current_thread);

//		debug("Saiu da funcao CWAIT pelo swap!\n");
	
		swap(current_thread, prio0, prio1, prio2, prio3);

		retorno = 0;
	}
	else 
		retorno = -1;
	

	sem->count--;
	
//	debug("Saiu da funcao CWAIT!\n");
	return retorno;
}


int csignal(csem_t *sem)
{
/*
- Incrementa count
- Checa se tem threads na fila de bloqueados do semaforo
	- Se sim: a primeira é removida
			o estado dela passas para apto
			ela é colocada na fila da prioridade correspondente
	- Se não: erro 
Retorno:
Quando executada corretamente: retorna 0 (zero)
Caso contrário, retorna um valor negativo
*/
//	debug("Entrou na funcao CSIGNAL!\n");
	
	int retorno = 0;
	TCB_t* thread;

	(sem->count)++;
	
	if(FirstFila2(sem->fila) == 0)
	{
		thread = GetAtIteratorFila2(sem->fila);

		DeleteAtIteratorFila2(sem->fila);

		thread->state = PROCST_APTO;

		insert_apto(thread, prio0, prio1, prio2, prio3);
	}
	else{
		debug("FILA DE THREADS BLOQUEADAS ESTA VAZIA!\n");
		retorno = -1;
	}

//	debug("Saiu da funcao CSIGNAL!\n");
	return retorno;
}

int cidentify (char *name, int size)
{
//	debug("Entrou na funcao CIDENTIFY!\n");
  	strncpy(name, "\nGiulia Bonaspetti Martins - 233677\nRenan Bortoluzzi- 240509\nTaiane de Oliveira Peixoto - 228369\n\0", (size_t)size);
//	debug("Saiu da funcao CIDENTIFY!\n");
	return 0;
}





