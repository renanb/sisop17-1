/*
 * cdata.h: arquivo de inclus�o de uso apenas na gera��o da libpithread
 *
 * Esse arquivo pode ser modificado. ENTRETANTO, deve ser utilizada a TCB fornecida
 *
 * Vers�o de 25/04/2017
 *
 */
#ifndef __cdata__
#define __cdata__

#include "support.h"

#define	PROCST_CRIACAO	0
#define	PROCST_APTO	1
#define	PROCST_EXEC	2
#define	PROCST_BLOQ	3
#define	PROCST_TERMINO	4

/* N�O ALTERAR ESSA struct */
typedef struct s_TCB { 
	int		tid; 		// identificador da thread
	int		state;		// estado em que a thread se encontra
					// 0: Cria��o; 1: Apto; 2: Execu��o; 3: Bloqueado e 4: T�rmino
	int 		ticket;		// "bilhete" de loteria da thread, para uso do escalonador
	ucontext_t 	context;	// contexto de execu��o da thread (SP, PC, GPRs e recursos) 
} TCB_t; 

//Estrutura auxiliar para bloqueados
typedef struct TCB_bck { 
	int		tid; 			// identificador da thread que ele esta esperando
	TCB_t	*thread_blocked;	// thread que esta bloqueada
} TCB_block; 

#define prio ticket

//Fun��es auxiliares
void terminate();

TCB_t* tid_exists(PFILA2 th_list, int tid);

TCB_block* tid_block_exists(PFILA2 th_list, int tid);

int find_current_thread(TCB_t** current_thread, PFILA2 threads);

int select_next_thread(TCB_t** next_thread, PFILA2 prio0, PFILA2 prio1, PFILA2 prio2, PFILA2 prio3);

void unlock_join(TCB_t* thread, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3);

void is_block(PFILA2 th_bk, int tid, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3);

void insert_apto(TCB_t* thread, PFILA2 p0, PFILA2 p1, PFILA2 p2, PFILA2 p3);

void swap(TCB_t* current_thread, PFILA2 prio0, PFILA2 prio1, PFILA2 prio2, PFILA2 prio3);

void print_list(PFILA2 list);


//Mesmas vari�veis da cthread.c s� que colocadas aqui tb para serem globais em todos arquivos
extern int is_main_thread;
extern int TID;
extern PFILA2 threads;
extern PFILA2 prio0;
extern PFILA2 prio1;
extern PFILA2 prio2;
extern PFILA2 prio3;
extern PFILA2 threads_block;
extern ucontext_t *terminate_context;

#endif
